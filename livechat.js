// This file is required by app.js. It sets up event listeners
// for the two main URL endpoints of the application
// and listens for socket.io messages.

// Export a function, so that we can pass 
// the app and io instances from the app.js file:

module.exports = function(app,io){

	// Initialize a new socket.io application, named 'chat'
	var chat = io.on('connection', function (socket) {

		// When the client emits the 'load' event, reply with the 
		// detail of chat room

		socket.on('load',function(data){

			var room = findClientsSocket(io,data.cid);

			if(room.length === 0 ) {

				socket.emit('peopleInChat', {
					cid: data.cid,
					number: 0
				});
			}
			else if(room.length === 1) {

				socket.emit('peopleInChat', {
					cid: data.cid,
					number: room.length,
					user: room[0].user
				});
			}
			else {
				chat.emit('tooMany',{
					cid:data.cid,
					number:room.length
				});
			}
		});

		// When the client emits 'participate', save him,
		// and add him to the room

		socket.on('participate', function(data) {

			var room = findClientsSocket(io, data.cid);
			// Only two people per room are allowed
			if (room.length < 2) {

				// Use the socket object to store data. Each client gets
				// their own unique socket object

				socket.user = data.user;
				socket.room = data.cid;

				// Add the client to the room
				socket.join(data.cid);

				if (room.length == 1) {

					var users = [];

					users.push(room[0].user);
					users.push(socket.user);

					// Send the startChat event to all the people in the
					// room, along with a list of people that are in it.

					chat.in(data.cid).emit('ready', {
						cid:data.cid,
						users: users
					});
				}
			}
			else {
				chat.emit('tooMany',{
					cid:data.cid,
					number:room.length
				});
			}
		});

		// Somebody left the chat
		socket.on('disconnect', function() {

			// Notify the other person in the chat room
			// that his partner has left

			socket.broadcast.to(this.room).emit('leave', {
				room: this.room,
				user: this.user
			});

			// leave the room
			socket.leave(socket.room);
		});


		// Handle the sending of messages
		socket.on('send', function(data){
			// When the server receives a message, it sends it to the other person in the room.
			socket.broadcast.to(socket.room).emit('receive',data);
		});
	});
};

function findClientsSocket(io,roomId, namespace) {
	var res = [],
		ns = io.of(namespace ||"/");    // the default namespace is "/"

	if (ns) {
		for (var id in ns.connected) {
			if(roomId) {
				var index = ns.connected[id].rooms.indexOf(roomId) ;
				if(index !== -1) {
					res.push(ns.connected[id]);
				}
			}
			else {
				res.push(ns.connected[id]);
			}
		}
	}
	return res;
}


